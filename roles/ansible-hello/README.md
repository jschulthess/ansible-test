ansible-hello
=========

A simple role printing a hello message with ansible user and host fqdn.

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

or:
    - hosts: servers
      tasks:
        - name: apply ansible-hello role
          include_role:
            name: ansible-hello


License
-------

BSD

Author Information
------------------

<schulthess@puzzle.ch>
