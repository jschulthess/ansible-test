ansible-test
============

A simple Ansible project useful for testing playbooks and roles

The project contains a simple playbook *pb-hello.yml* as starting point.
The playbook allows for testing connectivity and demostrates straight task usage as well as using a role.


License
-------

BSD

Author Information
------------------

<schulthess@puzzle.ch>
